package com.devcamp.customeraccountapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomeraccountApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomeraccountApiApplication.class, args);
	}

}
