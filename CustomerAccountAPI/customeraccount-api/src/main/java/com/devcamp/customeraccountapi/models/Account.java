package com.devcamp.customeraccountapi.models;

public class Account {
    int id;
    Customer customer;
    double balance = 0.0;
    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
    @Override
    public String toString() {
        //blance lam tron den chu thap phan thu 2
        double roundOff = (double) Math.round(balance*100)/100;
        return String.format("Name (id=%s) balance =$%s", id,roundOff) ;
    }
    
    public String getCustomerName(){
        return customer.getName();
    }

    public Account deposit(double amount){
        balance = balance+amount;
        return this;
    }

    public Account withdraw(double amount){
        if(balance>=amount){
            balance =balance-amount;
        }
        else{
            System.out.println("amount withdraw exceeds the current balence!");
        }
        return this;
    }
}
