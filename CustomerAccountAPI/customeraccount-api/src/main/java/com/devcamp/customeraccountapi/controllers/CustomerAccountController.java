package com.devcamp.customeraccountapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customeraccountapi.models.Account;
import com.devcamp.customeraccountapi.models.Customer;



@RestController
public class CustomerAccountController {
    @GetMapping("/accounts")
    public ArrayList<Account> getListAccount(){
        Customer customer1 = new Customer(1, "Hung", 10);
        Customer customer2 = new Customer(2, "Hoang", 20);
        Customer customer3 = new Customer(3, "Thuy", 30);

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        Account account1 = new Account(1, customer1, 200000);
        Account account2 = new Account(2, customer2, 400000);
        Account account3 = new Account(3, customer3, 500000);
        System.out.println(account1.toString());
        System.out.println(account2.toString());
        System.out.println(account3.toString());

        ArrayList<Account> listAccount = new ArrayList<>();
        listAccount.add( account3);
        listAccount.add( account2);
        listAccount.add( account1);
        return listAccount;
    }
}
