package com.devcamp.bookauthorapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapi.models.Author;
import com.devcamp.bookauthorapi.models.Book;

@RestController
@RequestMapping("/")
public class BookAuthorController {
    @GetMapping("/books")
    public ArrayList<Book> listBook(){
        Author author1 = new Author("Nguyen Viet Hung","HungNV",'m');
        System.out.println("Author 1:");
        System.out.println(author1);

        Author author2 = new Author("Nguyen Thi B", "BNT", 'f');
        System.out.println("Author 2:");
        System.out.println(author2);

        Author author3 = new Author("Nguyen Van A", "ANV", 'm');
        System.out.println("Author 3:");
        System.out.println(author3);

        Book book1 = new Book("Connnan", author1, 10.000, 5);
        System.out.println("Book 1:");
        System.out.println(book1);

        Book book2 = new Book("Doraemon", author2, 13.000, 7);
        System.out.println("Book 2:");
        System.out.println(book2);

        Book book3 = new Book("Shin", author1, 19.000, 1);
        System.out.println("Book 3:");
        System.out.println(book3);

        ArrayList<Book> listBook = new ArrayList<>();
        listBook.add(book1);
        listBook.add(book2);
        listBook.add(book3);

        return listBook;
    }
}
