package com.devcamp.bookauthorapiv2.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorapiv2.models.Author;
import com.devcamp.bookauthorapiv2.models.Book;

@RestController
public class BookAuthorController {
    @GetMapping("/books")
    public ArrayList<Book> getBooks() {
        Author author1 = new Author("Author1 Name", "author1@example.com", 'm');
        Author author2 = new Author("Author2 Name", "author2@example.com", 'f');
        Author author3 = new Author("Author3 Name", "author3@example.com", 'm');
        Author author4 = new Author("Author4 Name", "author4@example.com", 'f');
        Author author5 = new Author("Author5 Name", "author5@example.com", 'm');
        Author author6 = new Author("Author6 Name", "author6@example.com", 'f');

        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);
        System.out.println(author4);
        System.out.println(author5);
        System.out.println(author6);

        ArrayList<Author> authorList1 = new ArrayList<>();
        authorList1.add(author1);
        authorList1.add(author2);

        ArrayList<Author> authorList2 = new ArrayList<>();
        authorList2.add(author3);
        authorList2.add(author4);

        ArrayList<Author> authorList3 = new ArrayList<>();
        authorList3.add(author5);
        authorList3.add(author6);

        System.out.println(authorList1);
        System.out.println(authorList2);
        System.out.println(authorList3);

        Book book1 = new Book("Book1", authorList1, 20.000, 5);
        Book book2 = new Book("Book1", authorList2, 21.000, 7);
        Book book3 = new Book("Book1", authorList3, 22.000, 9);

        ArrayList<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        return books;
    }

}
