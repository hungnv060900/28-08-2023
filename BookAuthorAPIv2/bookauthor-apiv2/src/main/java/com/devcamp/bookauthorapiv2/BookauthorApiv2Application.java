package com.devcamp.bookauthorapiv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookauthorApiv2Application {

	public static void main(String[] args) {
		SpringApplication.run(BookauthorApiv2Application.class, args);
	}

}
