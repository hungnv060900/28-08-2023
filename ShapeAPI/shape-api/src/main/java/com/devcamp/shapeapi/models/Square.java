package com.devcamp.shapeapi.models;

public class Square extends Rectangle {
    double side;

    public Square(double width, double lenght) {
        super(width, lenght);
    }

    public Square(double side) {
        super(side, side);
    }

    public Square(String color, boolean filled, double width, double length) {
        super(color, filled, width, length);
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    public void setWidth(double side){
        this.side = width;
        this.side = lenght;
    }
    public void setLenght(double side){
        this.side = width;
        this.side = lenght;
    }

    @Override
    public String toString() {
        return "Square [Rectangle[Shape[color=" + color + "+,filled="+ filled +"], width= "+width+",lenght= "+lenght+"]";
    }
    
}
