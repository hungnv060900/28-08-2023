package com.devcamp.shapeapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shapeapi.models.Circle;
import com.devcamp.shapeapi.models.Rectangle;
import com.devcamp.shapeapi.models.Square;

@RestController
public class ShapeApiController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double circleArea(@RequestParam(required = true,name ="radius")double radius){
        Circle circle = new Circle(radius);
        
        return circle.getArea();
    }
    @GetMapping("/circle-perimeter")
    public double circlePerimeter(@RequestParam(required = true,name ="radius")double radius){
        Circle circle = new Circle(radius);
        
        return circle.getPerimeter();
    }
    @GetMapping("/rectangle-area")
    public double rectangleArea(@RequestParam(required = true,name ="width ")double width ,@RequestParam(required = true,name = "height")double height){
        Rectangle rectangle = new Rectangle(width, height);
        
        return rectangle.getArea();
    }
    @GetMapping("/rectangle-perimeter")
    public double rectanglePerimeter(@RequestParam(required = true,name ="width ")double width ,@RequestParam(required = true,name = "height")double height){
        Rectangle rectangle = new Rectangle(width, height);
        
        return rectangle.getPerimeter();
    }
    @GetMapping("/square-area")
    public double squareArea(@RequestParam(required = true,name ="spuare")double side){
        Square square = new Square(side);
        
        return square.getArea();
    }
    @GetMapping("/square-perimeter")
    public double squarePerimeter(@RequestParam(required = true,name ="spuare")double side){
        Square square = new Square(side);
        
        return square.getPerimeter();
    }
}
