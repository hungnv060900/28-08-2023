package com.devcamp.customerinvoiceapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerinvoiceapi.models.Customer;
import com.devcamp.customerinvoiceapi.models.Invoice;

@RestController
public class CustomerInvoiceController {
    @GetMapping("/invoices")
    public ArrayList<Invoice> getListInvoice() {
        Customer customer1 = new Customer(1, "Hung", 20);
        Customer customer2 = new Customer(2, "Hoang", 30);
        Customer customer3 = new Customer(3, "Thuy", 10);

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        Invoice invoice2 = new Invoice("HD002", customer1, 200000);
        Invoice invoice3 = new Invoice("HD003", customer1, 300000);
        Invoice invoice1 = new Invoice("HD001", customer1, 100000);

        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());
        System.out.println(invoice3.toString());

        ArrayList<Invoice> invoices = new ArrayList<>();
        invoices.add(invoice1);
        invoices.add(invoice2);
        invoices.add(invoice3);
        return invoices;
    }
}
