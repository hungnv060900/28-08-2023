package com.devcamp.customerinvoiceapi.models;

public class Invoice {
    String id;
    Customer customer;
    double amount;

    public Invoice(String id, Customer customer, double amount) {
        this.id = id;
        this.customer = customer;
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getCustomerID() {
        return this.customer.id;
    }

    public String getCustomerName() {
        return this.customer.name;
    }

    public int getCustomerDiscount() {
        return this.customer.discount;
    }

    public double getAmountAfterDiscount() {
        return amount - amount * customer.discount * 100;
    }

    @Override
    public String toString() {
        return String.format("Invoice[id=%s,customer=%s(%s)(discount %s%s),amount =%s]", id, customer.name, customer.id,
                customer.discount, amount);
    }

}
