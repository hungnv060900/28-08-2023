package com.devcamp.customerinvoiceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerinvoiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerinvoiceApiApplication.class, args);
	}

}
