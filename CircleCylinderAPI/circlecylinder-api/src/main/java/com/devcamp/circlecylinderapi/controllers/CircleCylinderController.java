package com.devcamp.circlecylinderapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.circlecylinderapi.models.Circle;
import com.devcamp.circlecylinderapi.models.Cylinder;

@RestController
@RequestMapping("/")
public class CircleCylinderController {
    @GetMapping("/circle-area")
    public double circleArea(@RequestParam(required = true,name = "radius") double radius){
        Circle circle = new Circle(radius);
        return circle.getArea(); 
    }
    @GetMapping("/cylinder-volume")
    public double cylinderVolume(@RequestParam(required = true,name = "radius") double radius,@RequestParam(required = true,name = "height") double height){
        Cylinder cylinder = new Cylinder(radius, height);
        return cylinder.getVolume();
    }
}
