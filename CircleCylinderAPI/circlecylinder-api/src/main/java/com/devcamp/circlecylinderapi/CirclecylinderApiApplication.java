package com.devcamp.circlecylinderapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CirclecylinderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CirclecylinderApiApplication.class, args);
	}

}
