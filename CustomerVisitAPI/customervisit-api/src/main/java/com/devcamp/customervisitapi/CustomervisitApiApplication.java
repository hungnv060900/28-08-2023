package com.devcamp.customervisitapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomervisitApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomervisitApiApplication.class, args);
	}

}
