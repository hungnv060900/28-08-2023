package com.devcamp.customervisitapi.controllers;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customervisitapi.models.Customer;
import com.devcamp.customervisitapi.models.Visit;

@RestController
public class CustomerVisitController {
    @CrossOrigin
    @GetMapping("/visits")
    public ArrayList<Visit> getListVisit(){
        Customer customer1 = new Customer("Hung", true, "Thuong gia");
        Customer customer2 = new Customer("Nguyen", false, "Binh dan");
        System.out.println("Customer 1:");
        System.out.println(customer1.toString());
        System.out.println("Customer 2:");
        System.out.println(customer2.toString());

        Date date = new Date();
        Visit visit1 = new Visit("Nha trang",date);
        System.out.println("Visit 1:");
        System.out.println(visit1.toString());
        Visit visit2 = new Visit("Da Lat",date);
        System.out.println("Visit 2:");
        System.out.println(visit2.toString());

        ArrayList<Visit> listVisits = new ArrayList<>();
        listVisits.add(visit1);
        listVisits.add( visit2);
        return listVisits;
    }
}
