package com.devcamp.animalapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.animalapi.models.Cat;
import com.devcamp.animalapi.models.Dog;

@RestController
public class AnimalController {
    @CrossOrigin
    @GetMapping("/cats")
    public ArrayList<Cat> getListCats(){
        Cat cat1 = new Cat("Tam the");
        System.out.println("Cat 1:");
        System.out.println(cat1.toString());
        cat1.greets();
        Cat cat2 = new Cat("Meo den");
        System.out.println("Cat 2:");
        System.out.println(cat2.toString());
        cat2.greets();
        Cat cat3 = new Cat("Meo meo");
        System.out.println("Cat 3:");
        System.out.println(cat3.toString());
        cat2.greets();
        ArrayList<Cat> listCats = new ArrayList<>();
        listCats.add(cat1);
        listCats.add(cat2);
        listCats.add(cat3);
        return listCats;
    }
    @GetMapping("/dogs")
    public ArrayList<Dog> getListDog(){
        Dog dog1 = new Dog("Vang");
        Dog dog2 = new Dog("Den");
        Dog dog3 = new Dog("Cam");
        System.out.println("Dog 1:");
        System.out.println(dog1.toString());
        System.out.println("Dog 2:");
        System.out.println(dog2.toString());
        System.out.println("Dog 3:");
        System.out.println(dog3.toString());
        dog1.greets();
        dog2.greets();
        dog3.greets();
        ArrayList<Dog> listDogs = new ArrayList<>();
        listDogs.add(dog1);
        listDogs.add(dog2);
        listDogs.add(dog3);
        return listDogs;
    }
}
