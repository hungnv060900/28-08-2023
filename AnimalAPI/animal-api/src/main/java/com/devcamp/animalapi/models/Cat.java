package com.devcamp.animalapi.models;

public class Cat extends Mammal {

    public Cat(String name) {
        super(name);
        // TODO Auto-generated constructor stub
    }

    public void greets() {
        System.out.println("Meow");
    }

    @Override
    public String toString() {
        return String.format("Cat[Mammal[Animal[name = %s]]]", name);
    }

}
